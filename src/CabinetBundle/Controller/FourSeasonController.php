<?php
/**
 *
 */

namespace CabinetBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class FourSeasonController extends Controller
{
    /**
     * @Template()
     * @return array
     * @Route("/", name="lk_four_seasons")
     */
    public function indexAction()
    {
        $customerRepository = $this->getDoctrine()->getRepository('EnotApiBundle:FourSeasonSession');
        $sessions = $customerRepository->findAll();

        return [
            'sessions' => $sessions
        ];
    }

    /**
     * @Template()
     * @param Request $request
     * @return array
     * @Route("/yandex", name="lk_yandex")
     */
    public function yandexAction(Request $request)
    {
        return [
            'phone' => $request->query->get("phone"),
            'amount' => $request->query->get("amount"),
        ];
    }
}