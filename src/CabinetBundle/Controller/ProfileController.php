<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CabinetBundle\Controller;

use FOS\UserBundle\Model\UserInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Controller\ProfileController as BaseController;

/**
 * Controller managing the user profile.
 *
 * @author Christophe Coevoet <stof@notk.org>
 */
class ProfileController extends BaseController
{
    /**
     * Show the user.
     */
    public function showAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $customerRepository = $this->getDoctrine()->getRepository('EnotApiBundle:Customer');
        $customer = $customerRepository->findOneBy(['user' => $user]);

        return $this->render('@FOSUser/Profile/show.html.twig', array(
            'user' => $user,
            'customer' => $customer
        ));
    }


    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @Template()
     */
    public function editAction(Request $request)
    {
        $customerRepository = $this->getDoctrine()->getRepository('EnotApiBundle:Customer');
        $customer = $customerRepository->findOneBy(['user' => $this->getUser()]);

        $editForm = $this->createForm('CabinetBundle\Form\CustomersType', $customer);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('fos_user_profile_show');
        }

        return $this->render('@FOSUser/Profile/edit.html.twig', [
            'customer' => $customer,
            'edit_form' => $editForm->createView()
        ]);
    }
}
