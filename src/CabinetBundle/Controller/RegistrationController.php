<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace CabinetBundle\Controller;

use Enot\ApiBundle\Entity\User;
use Enot\ApiBundle\Services\Main\EnotException;
use FOS\RestBundle\Controller\Annotations\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class RegistrationController extends Controller
{

    /**
     * @Template()
     * @Route("/", name="cabinet_register")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws EnotException
     * @throws \Enot\NotificationBundle\Model\SendSmsException
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('CabinetBundle\Form\RegistrationFormType', $user);
        $form->handleRequest($request);
        $error = null;

        if ($request->getMethod() == Request::METHOD_POST) {
            $username = $request->request->get("cabinet_register")["username"];
            $pin = $request->request->get("cabinet_register")["pin"];
            $this->_sendPinCode($username, $pin);
            return $this->redirectToRoute('register_confirmed');

        }

        return [
            'user' => $user,
            'form' => $form->createView(),
            'error' => $error
        ];
    }

    /**
     * @param Request $request
     * @param $phone
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws EnotException
     * @Route("/confirm/{phone}", name="register_confirm")
     * @Template()
     * @Method({"GET", "POST"})
     */
    public function confirmAction(Request $request, $phone)
    {
        $error = null;
        if ($request->isMethod('POST')) {
            if (array_key_exists('_confirm', $request->request->all())) {
                $code = (int)$request->request->get('_confirm');
                if ($this->_confirmUser($phone, $code)) {
                    return $this->redirectToRoute('register_confirmed');
                } else {
                    $error = "Не верный PIN код";
                }
            }
        }

        return [
            'error' => $error,
            'phone' => $phone
        ];
    }

    /**
     * @return array
     * @Route("/confirmed", name="register_confirmed")
     * @Template()
     */
    public function registerConfirmedAction()
    {
        return [
            'user' => new User()
        ];
    }

    /**
     * @param $phone
     * @param $pin
     * @throws EnotException
     */
    private function _sendPinCode($phone, $pin)
    {
        $userService = $this->get('enot_api.user_manager');
        $userService->sendPinCode($phone, $pin);
    }

    /**
     * @param $phone
     * @param $code
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws EnotException
     */
    private function _confirmUser($phone, $code)
    {
        $userService = $this->get('enot_api.user_manager');
        if ($userService->confirmUser("RU", $phone, $code)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $phone
     * @return null|string
     */
    private function _validatePhone($phone)
    {
        $userService = $this->get('enot_api.user_manager');
        $validatedPhone = $userService->validatePhone("RU", $phone);

        return $validatedPhone;
    }
}
