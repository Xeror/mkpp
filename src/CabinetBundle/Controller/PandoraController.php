<?php
namespace CabinetBundle\Controller;

use Enot\ChargingBundle\Entity\Station;
use Enot\ChargingBundle\Providers\PON\AlarmStatus;
use Enot\ChargingBundle\Providers\PON\PONConnector;
use Enot\ChargingBundle\Services\StationsManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class PandoraController extends Controller
{
    /**
     * @Template()
     * @return array
     * @throws \Enot\ApiBundle\Services\Main\EnotException
     * @Route("/", name="lk_pandora")
     */
    public function indexAction()
    {
        /** @var StationsManager $stationManager */
        $stationManager = $this->get("enot_api.charging.station_manager");

        /** @var Station[] $stations */
        $stations = $stationManager->getRepository()->getPandoraStations();

        $result = [];

        foreach ($stations as $station) {
            /** @var PONConnector $supplierConnector */
            $supplierConnector = $stationManager->getProvider($station)->getConnector();

            try{
                $alarmStatus = $supplierConnector->getAlarmStatus();
            } catch (\Exception $e) {
                $alarmStatus = new AlarmStatus();
            }

            $result[] = [
                'id' => $station->getId(),
                "phone" => $supplierConnector->getPhone() ? "******" . substr($supplierConnector->getPhone(), 6) : "",
                'gps' =>  "{$station->getLatitude()}, {$station->getLongitude()}",
                "status" => $alarmStatus
            ];
        }

        return [
            'stations' => $result
        ];
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Enot\ApiBundle\Services\Main\EnotException
     * @Route("/refresh_coords/{id}", name="lk_pandora_refresh_coords")
     */
    public function refreshCoords($id)
    {
        $stationManager = $this->get("enot_api.charging.station_manager");

        /** @var Station $station */
        $station = $this->getDoctrine()->getRepository('EnotChargingBundle:Station')->find($id);
        /** @var PONConnector $supplierConnector */
        $supplierConnector = $stationManager->getProvider($station)->getConnector();
        try {
            $response = $supplierConnector->getAlarmStatus();
            if($response->isOnline() && ($response->getLongitude() && $response->getLatitude())) {
                $station->setLatitude($response->getLatitude());
                $station->setLongitude($response->getLongitude());
                $this->getDoctrine()->getManager()->persist($station);
                $this->getDoctrine()->getManager()->flush();
            }
        } catch (\Exception $e) {

        }

        return $this->redirectToRoute("lk_pandora");
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Enot\ApiBundle\Services\Main\EnotException
     * @Route("/open/{id}", name="lk_pandora_open_cap")
     */
    public function openCapAction($id)
    {
        $stationManager = $this->get("enot_api.charging.station_manager");

        /** @var Station $station */
        $station = $this->getDoctrine()->getRepository('EnotChargingBundle:Station')->find($id);
        /** @var PONConnector $supplierConnector */
        $supplierConnector = $stationManager->getProvider($station)->getConnector();
        try {
            $supplierConnector->openCap();
        } catch (\Exception $e) {

        }

        return $this->redirectToRoute("lk_pandora");
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Enot\ApiBundle\Services\Main\EnotException
     * @Route("/start/{id}", name="lk_pandora_start")
     */
    public function startCharging($id)
    {
        $stationManager = $this->get("enot_api.charging.station_manager");

        /** @var Station $station */
        $station = $this->getDoctrine()->getRepository('EnotChargingBundle:Station')->find($id);
        /** @var PONConnector $supplierConnector */
        $supplierConnector = $stationManager->getProvider($station)->getConnector();
        try {
            $supplierConnector->preheatOn();
        } catch (\Exception $e) {

        }

        return $this->redirectToRoute("lk_pandora");
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Enot\ApiBundle\Services\Main\EnotException
     * @Route("/stop/{id}", name="lk_pandora_stop")
     */
    public function stopCharging($id)
    {
        $stationManager = $this->get("enot_api.charging.station_manager");

        /** @var Station $station */
        $station = $this->getDoctrine()->getRepository('EnotChargingBundle:Station')->find($id);
        /** @var PONConnector $supplierConnector */
        $supplierConnector = $stationManager->getProvider($station)->getConnector();
        try {
            $supplierConnector->stopCharging();
        } catch (\Exception $e) {

        }

        return $this->redirectToRoute("lk_pandora");
    }

}