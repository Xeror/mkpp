<?php

namespace CabinetBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Enot\ChargingBundle\Entity\Session;
use Enot\ChargingBundle\Repository\SessionRepository;
use FOS\RestBundle\Controller\Annotations\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PersonalAreaController extends Controller
{
    /**
     * @Template()
     * @return array
     * @Route("/", name="lk_homepage")
     */
    public function indexAction()
    {
        $customerRepository = $this->getDoctrine()->getRepository('EnotApiBundle:Customer');
        $customer = $customerRepository->findOneBy(['user' => $this->getUser()]);

        return [
            'customer' => $customer
        ];
    }

    /**
     * @Template()
     * @return array
     * @Route("/history/payments", name="lk_history_payments")
     */
    public function paymentHistoryAction()
    {
        $customersRepository = $this->getDoctrine()->getRepository('EnotApiBundle:Customer');
        $customer = $customersRepository->findOneBy(['user' => $this->getUser()]);

        $paymentsRepository = $this->getDoctrine()->getRepository('EnotChargingBundle:Payment');
        $chargingPayments = $paymentsRepository->findBy(['customer' => $customer], ['date' => 'DESC'], 10);

        /** @var EntityRepository $paymentsRepository */
        $paymentsRepository = $this->getDoctrine()->getRepository('EnotCarsharingBundle:Payment');
        $carsharingPayments = $paymentsRepository->createQueryBuilder('p')
            ->join('p.session', 's')
            ->where('s.customer = :customer')
            ->setParameter('customer', $customer)
            ->orderBy('p.date', 'DESC')
            ->getQuery()->getResult();

        $paymentsRepository = $this->getDoctrine()->getRepository('EnotParkingBundle:Payment');
        $payments = $paymentsRepository->createQueryBuilder('p')
            ->join('p.session', 's')
            ->where('s.customer = :customer')
            ->setParameter('customer', $customer)
            ->orderBy('p.date', 'DESC')
            ->getQuery()->getResult();

        return [
            'customer' => $customer,
            'chargingPayments' => $chargingPayments,
            'parkingPayments' => $payments,
            'carsharingPayments' => $carsharingPayments,
        ];
    }

    /**
     * @Template()
     * @return array
     * @Route("/history/charging", name="lk_history_charging")
     */
    public function chargingHistoryAction()
    {
        /** @var SessionRepository $sessionsRepository */
        $sessionsRepository = $this->getDoctrine()->getRepository('EnotChargingBundle:Session');
        $sessions = $sessionsRepository->findSessionsByUser($this->getUser());

        return [
            'sessions' => $sessions
        ];
    }

    /**
     * @Template()
     * @return array
     * @Route("/history/parking", name="lk_history_parking")
     */
    public function parkingHistoryAction()
    {
        $sessionsRepository = $this->getDoctrine()->getRepository('EnotParkingBundle:Session');
        $sessions = $sessionsRepository->findBy(['customer' => $this->getUser()->getCustomer()], ['id' => 'DESC']);

        return [
            'sessions' => $sessions
        ];
    }

    /**
     * @Template()
     * @param Session $session
     * @return array* @Route("/history/charging/{id}", name="lk_show_charging")
     */
    public function chargingShowAction(Session $session)
    {
        return [
            'session' => $session
        ];
    }

    /**
     * @Template()
     * @param \Enot\ParkingBundle\Entity\Session $session
     * @return array* @Route("/history/parking/{id}", name="lk_show_parking")
     */
    public function parkingShowAction(\Enot\ParkingBundle\Entity\Session $session)
    {
        return [
            'session' => $session
        ];
    }

    /**
     * @Route("/get_balance")
     * @param Request $request
     */
    public function getBalance(Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            $customerRepository = $this->getDoctrine()->getRepository('EnotApiBundle:Customer');
            $customer = $customerRepository->findOneBy(['user' => $this->getUser()]);

            $balance = $customer->getBalance();

            exit(json_encode($balance));
        }
    }
}
