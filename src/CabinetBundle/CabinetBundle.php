<?php

namespace CabinetBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CabinetBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
