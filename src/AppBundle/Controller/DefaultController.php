<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return array
     * @Template()
     */
    public function indexAction()
    {
        $news = $this->get("doctrine.orm.default_entity_manager")->getRepository("EnotApiBundle:News")->findBy([], [
            "id" => "DESC"
        ], 2);
        return ['news' => $news];
    }

    /**
     * @Route("/about", name="about")
     * @return array
     * @Template()
     */
    public function aboutAction()
    {
        return [];
    }

    /**
     * @Route("/account", name="account")
     * @return array
     * @Template()
     */
    public function accountAction()
    {
        return [];
    }
    /**
     *
     * @Route("/contacts", name="contacts")
     * @return array
     * @Template()
     */
    public function contactsAction()
    {
        return [];
    }

    /**
     *
     * @Route("/media-photo", name="media_photo")
     * @return array
     * @Template()
     */
    public function mediaPhotoAction()
    {
        return [];
    }

    /**
     *
     * @Route("/events", name="events")
     * @return array
     * @Template()
     */
    public function eventsAction()
    {
        $events = $this->get("doctrine.orm.default_entity_manager")->getRepository("EnotApiBundle:Event")->findAll();
        return ['events' => $events];
    }

    /**
     *
     * @Route("/event/{id}", name="event_get")
     * @param $id
     * @return array
     * @Template()
     */
    public function eventGetAction($id)
    {
        $event = $this->get("doctrine.orm.default_entity_manager")->getRepository("EnotApiBundle:Event")->find($id);
        return ['event' => $event];
    }

    /**
     *
     * @Route("/news/{id}", name="news_get")
     * @param $id
     * @return array
     * @Template()
     */
    public function newsGetAction($id)
    {
        $news = $this->get("doctrine.orm.default_entity_manager")->getRepository("EnotApiBundle:News")->find($id);
        return ['news' => $news];
    }

    /**
     *
     * @Route("/members", name="members")
     * @return array
     * @Template()
     */
    public function membersAction()
    {
        return [];
    }

    /**
     *
     * @Route("/news", name="news")
     * @return array
     * @Template()
     */
    public function newsAction()
    {
        $news = $this->get("doctrine.orm.default_entity_manager")->getRepository("EnotApiBundle:News")->findBy([], [
            "id" => "DESC"
        ]);
        return ['news' => $news];
    }

    /**
     *
     * @Route("/search", name="search")
     * @return array
     * @Template()
     */
    public function searchAction()
    {
        return [];
    }
    /**
     *
     * @Route("/progress", name="progress")
     * @return array
     * @Template()
     */
    public function progressAction()
    {
        return [];
    }

    /**
     *
     * @Route("/media", name="media")
     * @return array
     * @Template()
     */
    public function mediaAction()
    {
        $news = $this->get("doctrine.orm.default_entity_manager")->getRepository("EnotApiBundle:News")->findBy([], [
            "id" => "DESC"
        ], 3);
        return ['news' => $news];
    }

    /**
     *
     * @Route("/new-partner", name="new_partner")
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function newPartnerAction(Request $request)
    {
        $mailer = $this->get("swiftmailer.mailer");
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('errsharing@evserver.ru')
            ->setTo('membership@ic-ie.com')
            ->setBody(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'emails/registration.html.twig',
                    ["vars" => $request->request->all()]
                ),
                'text/html'
            );
        $mailer->send($message);
        return $this->redirectToRoute('members');
    }

    /**
     * @Route("/app", name="application")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function applicationAction(Request $request)
    {
        $userAgent = $request->server->get('HTTP_USER_AGENT');

        if (is_int(strpos($userAgent, 'Android'))) {
            return $this->redirect('https://play.google.com/store/apps/details?id=ru.mos.enot.chargers');
        } else {
            return $this->redirect('https://itunes.apple.com/ru/app/%D0%BC%D0%BE%D1%81%D1%8D%D0%BD%D0%B5%D1%80%D0%B3%D0%BE%D1%81%D0%B1%D1%8B%D1%82-%D1%8D%D0%BB%D0%B5%D0%BA%D1%82%D1%80%D0%BE%D0%B7%D0%B0%D1%80%D1%8F%D0%B4%D0%BA%D0%B8/id1381109935?mt=8');
        }
    }
}
