<?php

namespace Enot\ApiBundle\Services;


use Enot\ApiBundle\Services\Main\EnotException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CdnManager
{
    /** @var string */
    private $url;

    /** @var string */
    private $username;

    /** @var string */
    private $password;

    /** @var HttpClient */
    private $httpClient;

    public function __construct(string $url, string $username, string $password, HttpClient $httpClient)
    {
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;
        $this->httpClient = $httpClient;
    }

    public function loadFile(UploadedFile $file, $prefix)
    {
        $conn_id = ftp_connect($this->url);
        ftp_login($conn_id, $this->username, $this->password);
        ftp_pasv($conn_id, true);
        ftp_put($conn_id, $prefix . $file->getClientOriginalName(), $file->getPathname(), FTP_IMAGE);
        return $prefix . $file->getClientOriginalName();
    }

    /**
     * @param $url
     * @return string
     */
    public function get($url)
    {
        $response = null;
        try {
            $response = $this->httpClient->get($url, "", "", "image/jpeg", [
                CURLOPT_USERPWD => "{$this->username}:{$this->password}",
                CURLOPT_HTTPAUTH => CURLAUTH_BASIC
            ]);
        } catch (EnotException $ex) {

        }
        header('Content-type: image/jpeg');
        return $response;
    }
}