<?php
/**
 * ...
 */

namespace Enot\ApiBundle\Services;

use Doctrine\ORM\EntityManager;
use Enot\ApiBundle\Entity\Customer;

use Enot\ApiBundle\Entity\CustomerVehicle;
use Enot\ApiBundle\Repository\CustomerRepository;
use Enot\ApiBundle\Services\Main\EnotException;
use Enot\ApiBundle\Utils\EnotError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerManager
{
    const
        AUTH_STATUS_ACCEPTED_ID = 1,
        AUTH_STATUS_BLOCKED_ID = 2,
        AUTH_STATUS_EXPIRED_ID = 3,
        AUTH_STATUS_INVALID_ID = 4,
        AUTH_STATUS_CONCURRENT_ID = 5;

    /** @var EntityManager $em */
    protected $em;

    /** @var Customer $em */
    private $customer;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @return CustomerRepository
     */
    public function getRepository()
    {
        /** @var CustomerRepository $repository */
        $repository = $this->em->getRepository('EnotApiBundle:Customer');
        return $repository;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    public function getAuthStatusRepository()
    {
        return $this->em->getRepository('EnotChargingBundle:AuthorizationStatus');
    }

    /**
     * @param Request $request
     * @return Customer
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function edit(Request $request)
    {
        if ($request->request->get("first_name")) {
            $this->getCustomer()->setFirstName($request->request->get("first_name"));
        }

        if ($request->request->get("second_name")) {
            $this->getCustomer()->setSecondName($request->request->get("second_name"));
        }

        $this->save($this->getCustomer());
        return $this->getCustomer();
    }

    /**
     * @param $id
     * @return bool
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     */
    public function removeVehicle($id)
    {
        $vehicle = $this->em->getRepository("EnotApiBundle:CustomerVehicle")->find($id);

        if(!$vehicle) {
            throw new EnotException(EnotError::WRONG_PARAMETERS, '', Response::HTTP_BAD_REQUEST);
        }

        $this->em->remove($vehicle);
        $this->em->flush($vehicle);

        return true;
    }

    /**
     * @param $vehicleId
     * @param $modelId
     * @param $carNumber
     * @return CustomerVehicle|null|object
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function editVehicle($vehicleId, $modelId, $carNumber)
    {
        $vehicle = $this->em->getRepository("EnotApiBundle:CustomerVehicle")->find($vehicleId);

        if(!$vehicle) {
            throw new EnotException(EnotError::WRONG_PARAMETERS, '', Response::HTTP_BAD_REQUEST);
        }

        if($modelId){
            $model = $this->em->getRepository("EnotApiBundle:VehicleModel")->find($modelId);
            if(!$model) {
                throw new EnotException(EnotError::WRONG_PARAMETERS, '', Response::HTTP_BAD_REQUEST);
            }

            $vehicle->setModel($model);
        }

        if($carNumber) {
            $vehicle->setCarNumber($carNumber);
        }


        $this->save($vehicle);
        return $vehicle;
    }

    /**
     * @param $modelId
     * @param $carNumber
     * @return CustomerVehicle
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function addVehicle($modelId, $carNumber)
    {
        $model = $this->em->getRepository("EnotApiBundle:VehicleModel")->find($modelId);

        if(!$model) {
            throw new EnotException(EnotError::WRONG_PARAMETERS, '', Response::HTTP_BAD_REQUEST);
        }

        $vehicle = new CustomerVehicle();
        $vehicle->setCustomer($this->getCustomer());
        $vehicle->setModel($model);
        $vehicle->setCarNumber($carNumber);
        $this->save($vehicle);

        return $vehicle;
    }


    /**
     * @param object $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function save($entity)
    {
        $this->em->persist($entity);
        $this->em->flush($entity);
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     * @return CustomerManager
     */
    public function setCustomer(Customer $customer): CustomerManager
    {
        $this->customer = $customer;

        return $this;
    }
}