<?php

namespace Enot\ApiBundle\Services\PaymentSystem;

use Enot\ApiBundle\Entity\User;
use Enot\ChargingBundle\Services\PaymentManager;
use GuzzleHttp\Client;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class OrangeManager
{
    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var string
     */
    private $serverUrl;

    /**
     * @var string
     */
    private $serverToken;


    public function __construct(PaymentManager $paymentManager,
                                string $serverUrl,
                                string $serverToken)
    {
        $this->paymentManager = $paymentManager;
        $this->serverUrl = $serverUrl;
        $this->serverToken = $serverToken;
    }

    /**
     * @param $data
     * @return string
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Enot\ApiBundle\Services\Main\EnotException
     */
    public function topUp($data)
    {
        $orderId = $data['charge']['id'];
        $phone = $data['charge']['attributes']['description'];
        $amount = $data['charge']['attributes']['total_amount'];
        $currency = $data['charge']['attributes']['currency'];
        $status = $data['charge']['attributes']['status'];


        if ($status === 'successful') {
            $result = $this->paymentManager->customersTopUp($phone, $amount, $orderId, $currency);
        } else {
            $result = $status;
        }

        return $result;
    }

    /**
     * @param User $user
     * @param float $amount
     * @param string $currency
     * @return mixed
     */
    public function getPaymentForm($user, float $amount, string $currency)
    {
        if ($amount <= 0) {
            throw new BadRequestHttpException();
        }

        $data = [
            'amount' => $amount,
            'currency' => $currency,
            'pay_method' => 'card',
            'description' => $user->getUsername(),
            'email' => $user->getEmail()
        ];

        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . $this->serverToken
        ];
        
        $client = new Client();
        $response = $client->post($this->serverUrl, ['json' => $data, 'headers' => $headers, 'connect_timeout' => 5]);

        return json_decode((string)$response->getBody()->getContents(), true);
    }
}