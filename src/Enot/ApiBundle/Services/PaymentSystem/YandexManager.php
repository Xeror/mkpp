<?php
/**
 *
 */

namespace Enot\ApiBundle\Services\PaymentSystem;

use Enot\ApiBundle\Entity\User;
use Enot\ChargingBundle\Services\PaymentManager;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Request;

class YandexManager
{
    const STATUS_WAITING = "waiting_for_capture",
        STATUS_SUCCESS = "succeeded";
    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $url;

    public function __construct(PaymentManager $paymentManager,
                                string $login,
                                string $password,
                                string $url)
    {
        $this->paymentManager = $paymentManager;
        $this->login = $login;
        $this->password = $password;
        $this->url = $url;
    }

    /**
     * @param User $user
     * @param $value
     * @param $currency
     * @return mixed
     */
    public function create($user, $value, $currency)
    {
        $request = [
            'amount' => [
                'value' => $value,
                'currency' => $currency
            ],
            'payment_method_data' => [
                'type' => 'yandex_money'
            ],
            'confirmation' => [
                'type' => 'redirect',
                'return_url' => 'http://ya.ru'
            ],
            'description' => $user->getUsername()
        ];

        $headers = [
            'Idempotence-Key' => uniqid(),
            'Content-Type' => 'application/json'
        ];

        $client = new Client();
        $response = $client->post($this->url, ['json' => $request, 'headers' => $headers, 'connect_timeout' => 5,
            'curl' => [CURLOPT_USERPWD => "{$this->login}:{$this->password}"]]);
        return json_decode((string)$response->getBody()->getContents(), true);
    }

    /**
     * @param $object
     * @return mixed
     */
    public function confirm($object)
    {
        $paymentId = $object['id'];
        $value = $object['amount']['value'];
        $currency = $object['amount']['currency'];

        $json = [
            'amount' => [
                'value' => $value,
                'currency' => $currency
            ],
        ];

        $headers = [
            'Idempotence-Key' => uniqid(),
            'Content-Type' => 'application/json'
        ];

        $client = new Client();

        $response = $client->post($this->url . "/{$paymentId}/capture",
            ['json' => $json, 'headers' => $headers, 'connect_timeout' => 5,
                'curl' => [CURLOPT_USERPWD => "{$this->login}:{$this->password}"]]);

        return json_decode((string)$response->getBody()->getContents(), true);
    }

    /**
     * @param $object
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Enot\ApiBundle\Services\Main\EnotException
     */
    public function topUp($object)
    {
        $orderId = $object['id'];
        $phone = $object['description'];
        $amount = $object['amount']['value'];
        $currency = $object['amount']['currency'];

        $result = $this->paymentManager->customersTopUp($phone, $amount, $orderId, $currency);

        return $result;
    }

    /**
     * @param $phone
     * @param $amount
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Enot\ApiBundle\Services\Main\EnotException
     */
    public function topUpCustom($phone, $amount)
    {
        $result = $this->paymentManager->customersTopUp($phone, $amount, uniqid());

        return $result;
    }
}