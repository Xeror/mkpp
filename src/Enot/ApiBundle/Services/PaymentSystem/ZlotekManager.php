<?php
/**
 *
 */

namespace Enot\ApiBundle\Services\PaymentSystem;

use Doctrine\ORM\EntityManager;
use Enot\ApiBundle\Entity\Customer;
use Enot\ApiBundle\Entity\ZlotekCustomer;
use Enot\ApiBundle\Services\Main\EnotException;
use Enot\ApiBundle\Utils\EnotError;
use Enot\ChargingBundle\Services\PaymentManager;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;

class ZlotekManager
{
    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var ZlotekCustomer
     */
    private $customer;

    /**
     * @var Customer
     */
    private $apiCustomer;

    /**
     * @var string
     */
    private $url;

    public function __construct(PaymentManager $paymentManager,
                                EntityManager $entityManager,
                                string $url)
    {
        $this->paymentManager = $paymentManager;
        $this->entityManager = $entityManager;
        $this->url = $url;
    }

    /**
     * @return ZlotekCustomer
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws EnotException
     */
    public function createUser()
    {
        $client = new Client();
        $response = $client->post("{$this->url}/jcc/user/create", ['json' => [
            "phone" => $this->apiCustomer->getUser()->getUsername()
        ],
            'headers' => [
            ],
            'connect_timeout' => 5,
            'curl' => [CURLOPT_SSL_VERIFYHOST => false, CURLOPT_SSL_VERIFYPEER => false]
        ]);

        $response = json_decode((string)$response->getBody()->getContents(), true);
        if ($response['password']) {
            $newCustomer = new ZlotekCustomer();
            $newCustomer->setCustomer($this->apiCustomer);
            $newCustomer->setPassword($response['password']);
            $this->entityManager->persist($newCustomer);
            $this->entityManager->flush($newCustomer);
            return $newCustomer;
        }
        throw new EnotException(EnotError::WRONG_PARAMETERS, '', Response::HTTP_BAD_REQUEST);
    }

    /**
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createCard()
    {
        $this->login();
        $client = new Client();
        $response = $client->post("{$this->url}/jcc/card/create", ['json' => [
            "amount" => 1,
            "currency" => 978
        ],
            'headers' => [
                "phone" => $this->customer->getCustomer()->getUser()->getUsername(),
                "password" => $this->customer->getPassword()
            ],
            'connect_timeout' => 5,
            'curl' => [CURLOPT_SSL_VERIFYHOST => false, CURLOPT_SSL_VERIFYPEER => false]
        ]);
        $response = json_decode((string)$response->getBody()->getContents(), true);
        return $response;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws EnotException
     */
    public function login()
    {
        if (!$this->customer) {
            $repository = $this->entityManager->getRepository("EnotApiBundle:ZlotekCustomer");
            $this->customer = $repository->findOneBy([
                'customer' => $this->apiCustomer
            ]);
        }

        if (!$this->customer) {
            $this->customer = $this->createUser();
        }
    }


    /**
     * @param $value
     * @param $currency
     * @return mixed
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function create($value, $currency)
    {
        $currency = 978;
        $this->login();
        $cardId = $this->get();
        $client = new Client();
        $response = $client->post("{$this->url}/jcc/payment/create", ['json' => [
            "amount" => $value,
            "cardId" => $cardId['id'],
            "purchaseCurrency" => $currency
        ],
            'headers' => [
                "phone" => $this->customer->getCustomer()->getUser()->getUsername(),
                "password" => $this->customer->getPassword()
            ],
            'connect_timeout' => 5,
            'curl' => [CURLOPT_SSL_VERIFYHOST => false, CURLOPT_SSL_VERIFYPEER => false]
        ]);
        $response = json_decode((string)$response->getBody()->getContents(), true);
        return $response;
    }

    /**
     * @return mixed
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function get()
    {
        $this->login();
        $client = new Client();
        $response = $client->get("{$this->url}/jcc/card/get", ['json' => [],
            'headers' => [
                "phone" => $this->customer->getCustomer()->getUser()->getUsername(),
                "password" => $this->customer->getPassword()
            ],
            'connect_timeout' => 5,
            'curl' => [CURLOPT_SSL_VERIFYHOST => false, CURLOPT_SSL_VERIFYPEER => false]
        ]);
        $response = json_decode((string)$response->getBody()->getContents(), true);
        if(!count($response)) {
            throw new EnotException(EnotError::WRONG_PARAMETERS, '', Response::HTTP_BAD_REQUEST);
        }
        return current($response);
    }

    /**
     * @return mixed
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function getPayments()
    {
        $this->login();
        $client = new Client();
        $response = $client->get("{$this->url}/jcc/payment/get", ['json' => [],
            'headers' => [
                "phone" => $this->customer->getCustomer()->getUser()->getUsername(),
                "password" => $this->customer->getPassword()
            ],
            'connect_timeout' => 5,
            'curl' => [CURLOPT_SSL_VERIFYHOST => false, CURLOPT_SSL_VERIFYPEER => false]
        ]);
        $response = json_decode((string)$response->getBody()->getContents(), true);

        return $response['payments'];
    }

    /**
     * @param $object
     * @return bool
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Enot\ApiBundle\Services\Main\EnotException
     */
    public function topUp($object)
    {
        $orderId = $object['id'];
        $phone = $object['description'];
        $amount = $object['amount']['value'];
        $currency = $object['amount']['currency'];

        $result = $this->paymentManager->customersTopUp($phone, $amount, $orderId, $currency);

        return $result;
    }

    /**
     * @param $phone
     * @param $amount
     * @param $orderId
     * @param $secret
     * @return bool
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function topUpCustom($phone, $amount, $orderId, $secret)
    {
        $validationString = $phone . '&' . $amount . '&' . $orderId . '&' . GatewayManager::SALT;
        $validationSecret = sha1($validationString);

        if ($validationSecret !== $secret) {
            throw new EnotException(EnotError::SECRET_NOT_VALID, '', Response::HTTP_BAD_REQUEST);
        }

        $result = $this->paymentManager->customersTopUp($phone, $amount, uniqid());

        return $result;
    }

    public function setCustomer(Customer $customer)
    {
        $this->apiCustomer = $customer;

        return $this;
    }
}