<?php

namespace Enot\ApiBundle\Services\PaymentSystem;

use Enot\ApiBundle\Entity\User;
use Enot\ApiBundle\Services\Main\EnotException;
use Enot\ApiBundle\Utils\EnotError;
use Enot\ChargingBundle\Services\PaymentManager;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class GatewayManager
{
    const SALT = "hawlKGl1PW";

    /**
     * @var PaymentManager
     */
    private $paymentManager;

    /**
     * @var string
     */
    private $serverUrl;


    public function __construct(PaymentManager $paymentManager,
                                string $serverUrl)
    {
        $this->paymentManager = $paymentManager;
        $this->serverUrl = $serverUrl;
    }

    /**
     * @param $data
     * @return string
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function topUp($data)
    {
        $phone = $data['Phone'];
        $amount = $data['Amount'];
        $orderId = $data['OrderID'];
        $secret = $data['Secret'];

        $validationString = $phone . '&' . $amount . '&' . $orderId . '&' . self::SALT;
        $validationSecret = sha1($validationString);

        if ($validationSecret !== $secret) {
            throw new EnotException(EnotError::SECRET_NOT_VALID, '', Response::HTTP_BAD_REQUEST);
        }

        if (empty($phone) || empty($amount) || empty($orderId) || !is_numeric($amount)) {
            throw new EnotException(EnotError::WRONG_PARAMETERS, '', Response::HTTP_BAD_REQUEST);
        }

        $result = $this->paymentManager->customersTopUp($phone, $amount, $orderId);

        return $result;
    }

    /**
     * @param User $user
     * @param float $amount
     * @param string $currency
     * @return mixed
     */
    public function getPaymentForm($user, float $amount, string $currency)
    {
        if ($amount <= 0) {
            throw new BadRequestHttpException();
        }

        $data = [
            'phone' => $user->getUsername(),
            'amount' => $amount
        ];

        $headers = [
            'Content-Type' => 'application/json',
        ];

        $client = new Client();
        $response = $client->post($this->serverUrl, ['json' => $data, 'headers' => $headers, 'connect_timeout' => 5]);
        return $response->getBody()->getContents();
    }
}