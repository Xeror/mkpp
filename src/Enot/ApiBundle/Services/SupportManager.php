<?php
/**
 * ...
 */

namespace Enot\ApiBundle\Services;


use Doctrine\ORM\EntityManager;
use Enot\ApiBundle\Entity\Customer;
use Enot\ApiBundle\Entity\User;
use Enot\NotificationBundle\Services\NotificationManager;

class SupportManager
{
    /**
     * @var NotificationManager
     */
    private $notificationManager;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var string
     */
    private $supportEmailAddress;

    public function __construct(NotificationManager $notificationManager,
                                EntityManager $entityManager,
                                $supportEmailAddress)
    {
        $this->notificationManager = $notificationManager;
        $this->entityManager = $entityManager;
        $this->supportEmailAddress = $supportEmailAddress;
    }

    /**
     * @param User $user
     * @return Customer|null|object
     */
    public function getCustomer(User $user)
    {
        return $this->entityManager->getRepository('EnotApiBundle:Customer')->findOneByUser($user);
    }

    /**
     * @param User $user
     * @param null|string $title
     * @param null|string $content
     * @return string
     */
    public function contactEmail(User $user, ?string $title, ?string $content)
    {
        $message = $this->fillEmail($user, $content);

        $notification = $this->notificationManager->get(NotificationManager::NOTIFICATION_EMAIL);
        $notification->send($this->supportEmailAddress, $title, $message);

        return ResponseManager::STATUS_SUCCESS;
    }

    /**
     * @param User $user
     * @param $content
     * @return string
     */
    private function fillEmail(User $user, $content)
    {
        $message = '';
        $customer = $this->getCustomer($user);
        $message .=
            "<strong>Login: </strong>{$user->getUsername()}<br>" .
            "<strong>First name: </strong>{$customer->getFirstName()}<br>" .
            "<strong>Second name: </strong>{$customer->getSecondName()}<br><br>" .
            "<strong>Message: </strong>$content";

        return $message;
    }
}