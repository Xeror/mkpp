<?php
/**
 * ...
 */

namespace Enot\ApiBundle\Services;

use Enot\ApiBundle\Entity\User;
use Enot\ApiBundle\Services\Main\EnotException;
use Enot\ApiBundle\Services\Main\HttpClientInterface;
use Enot\NotificationBundle\Services\NotificationManager;
use Enot\NotificationBundle\Services\SmsManager;
use \FOS\UserBundle\Doctrine\UserManager as FosUserManager;
use Doctrine\ORM\EntityManager;
use Enot\ApiBundle\Entity\Phone;
use Enot\ApiBundle\Utils\EnotError;
use FOS\UserBundle\Util\UserManipulator;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Response;

class UserManager
{
    const
        OPTION_SMS_API_ID = 'sms_api_id',
        OPTION_SMS_URL = 'sms_url',
        DEFAULT_PIN_CODE = 5555,
        FAKE_PHONE = "79789999999",
        EXCEPTION_PHONES = ['79268775840', '79781126466'];

    /** @var EntityManager|null */
    private $entityManager = null;

    /** @var FosUserManager|null */
    private $fosUserManager = null;

    /** @var HttpClientInterface|null */
    private $httpClient = null;

    /** @var EventDispatcher|null */
    private $userManipulator = null;

    /**
     * @param EntityManager $entityManager
     * @param FosUserManager $userManager
     * @param HttpClientInterface $httpClient
     * @param NotificationManager $notificationManager
     * @param UserManipulator $userManipulator
     */
    public function __construct(EntityManager $entityManager,
                                FosUserManager $userManager,
                                HttpClientInterface $httpClient,
                                UserManipulator $userManipulator)
    {
        $this->entityManager = $entityManager;
        $this->fosUserManager = $userManager;
        $this->httpClient = $httpClient;
        $this->userManipulator = $userManipulator;
    }

    /**
     * @param $countryCode
     * @param $phone
     * @param $pin
     * @return string
     * @throws EnotException
     */
    public function sendPinCode($phone, $pin)
    {

        $email = $phone . '@evserver.ru';
        $this->createUser($phone, $email, $pin);

        return true;
    }

    /**
     * @param $countryCode
     * @param $phone
     * @param $code
     * @return bool
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     */
    public function confirmUser($countryCode, $phone, $code)
    {
        if (!isset($phone) || !isset($code) || !isset($countryCode)) {
            throw new EnotException(EnotError::WRONG_PHONE_OR_CODE, '', Response::HTTP_BAD_REQUEST);
        }

        $validatedPhone = $this->validatePhone((string)$countryCode, (string)$phone);

        if (!isset($validatedPhone)) {
            throw new EnotException(EnotError::WRONG_PHONE, '', Response::HTTP_BAD_REQUEST);
        }

        $existUser = $this->fosUserManager->findUserByUsername($validatedPhone);
        if (!$existUser) {
            throw new EnotException(EnotError::WRONG_PHONE, '', Response::HTTP_BAD_REQUEST);
        }

        if ($existUser->getConfirmationToken() != $code) {
            throw new EnotException(EnotError::WRONG_CODE, '', Response::HTTP_BAD_REQUEST);
        }

        $existUser->setPlainPassword($code);
        $existUser->setEnabled(true);
        $existUser->setConfirmationToken(null);
        $this->fosUserManager->updateUser($existUser);

        $this->addPhone($existUser->getId(), $validatedPhone);

        return ResponseManager::STATUS_SUCCESS;
    }

    /**
     * Возвращает информацию о пользователе
     *
     * @param User $user
     * @return mixed
     * @throws EnotException
     */
    public function getCustomerInfo(User $user)
    {
        if (isset($user)) {
            $customersRepository = $this->entityManager->getRepository('EnotApiBundle:Customer');
            $customer = $customersRepository->findOneBy(['user' => $user]);

            return $customer;
        } else {
            throw new EnotException(EnotError::WRONG_TOKEN, '', Response::HTTP_BAD_REQUEST);
        }
    }

    /**
     * Return 10-numeric phone number without country code or null
     *
     * @param $countryCode
     * @param string $phone
     * @return null|string
     */
    public function validatePhone($countryCode, $phone)
    {
        $result = null;

        $countryCodeList = $this->httpClient->get('http://country.io/phone.json', null, null);
        $countryCodeList = json_decode($countryCodeList);

        if (!property_exists($countryCodeList, $countryCode)) {
            return $result;
        }
        $countryCodeValue = $countryCodeList->$countryCode;

        //phone number without non-numeric caracters
        $clearPhone = preg_replace("/[^0-9]/", "", $phone);

        if (strlen($clearPhone) === 10) {
            $result = $countryCodeValue . $clearPhone;
        }


        return $result;
    }


    /**
     * Create user and set pinCode to confirmationToken field. Return pinCode
     *
     * @param $username
     * @param $email
     * @param $pin
     * @return int
     */
    private function createUser($username, $email, $pin)
    {
//
//        if ($this->isException($username)) {
//            $pin = self::DEFAULT_PIN_CODE;
//        }

        $user = $this->fosUserManager->findUserByUsername($username);
        if (!isset($user)) {
            $secretPassword = substr(md5(microtime(true) * rand(0, 9999)), 0, 20);
            /** @var User $user */
            $user = $this->userManipulator->create(
                $username,
                $secretPassword,
                $email,
                false,
                false
            );
        }

        $user->setConfirmationToken($pin);
        $user->setPlainPassword($pin);
        $user->setEnabled(true);
        $user->setConfirmationToken(null);
        $this->fosUserManager->updateUser($user);

        return $pin;
    }

    /**
     * @return int
     */
    private function generatePinCode()
    {
        $pinCode = rand(1111, 9999);
        return $pinCode;
    }

    /**
     * @param $phone
     * @param $message
     * @param $code
     * @return bool
     */
    private function sendSms($phone, $message, $code)
    {
//        /** @var SmsManager $smsManager */
//        $smsManager = $this->notificationManager->get(NotificationManager::NOTIFICATION_SMS);
//        return $smsManager->send($phone, null, $message, [
//            'code' => $code
//        ]);
    }

    /**
     * @param $userId
     * @param $phone
     * @throws \Doctrine\ORM\ORMException
     */
    private function addPhone($userId, $phone)
    {
        $user = $this->entityManager->getRepository('EnotApiBundle:User')->find($userId);
        $customerRepository = $this->entityManager->getRepository('EnotApiBundle:Customer');
        $customer = $customerRepository->findOneBy(['user' => $user]);

        $phoneRepository = $this->entityManager->getRepository('EnotApiBundle:Phone');
        $existPhone = $phoneRepository->findOneBy(['number' => $phone]);

        if (!isset($existPhone)) {
            $newPhone = new Phone();
            $newPhone->setCustomer($customer);
            $newPhone->setNumber($phone);

            $this->entityManager->persist($newPhone);
            $this->entityManager->flush($newPhone);
        }
    }

    public function isException($username)
    {
        return in_array($username, self::EXCEPTION_PHONES) || $username == self::FAKE_PHONE;
    }
}