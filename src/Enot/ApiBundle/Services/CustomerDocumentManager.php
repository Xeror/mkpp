<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 2018-12-28
 * Time: 15:17
 */

namespace Enot\ApiBundle\Services;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Enot\ApiBundle\Entity\Customer;
use Enot\ApiBundle\Entity\CustomerDocument;
use Enot\ApiBundle\Entity\CustomerDocumentType;
use Enot\ApiBundle\Services\Main\EnotException;
use Enot\ApiBundle\Utils\EnotError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CustomerDocumentManager extends CdnManager
{
    const DOCUMENT_TYPE_CYPRUS_ID = 1,
        DOCUMENT_TYPE_DRIVING_LICENSE = 2,
        DOCUMENT_TYPE_SELFIE = 3;

    const DOCUMENT_STATUS_AWAITING = 1,
            DOCUMENT_STATUS_ON_CHECK = 2;
    /** @var EntityManager */
    private $entityManager;

    public function __construct(string $url, string $username, string $password, HttpClient $httpClient, $entityManager)
    {
        parent::__construct($url, $username, $password, $httpClient);
        $this->entityManager = $entityManager;
    }

    /**
     * @param Customer $customer
     * @param Request $request
     * @return CustomerDocument
     * @throws EnotException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function load(Customer $customer, Request $request)
    {
        /** @var UploadedFile $image */
        $image = $request->files->get('image');
        $type = $request->request->get('typeId');

        /** @var CustomerDocumentType $type */
        $type = $this->getRepository()->find($type);

        if(!$image instanceof UploadedFile){
            throw new EnotException(EnotError::WRONG_IMAGE, '', Response::HTTP_BAD_REQUEST);
        }

        if (!$type) {
            throw new EnotException(EnotError::WRONG_PARAMETERS, '', Response::HTTP_BAD_REQUEST);
        }

        $document = $this->entityManager->getRepository("EnotApiBundle:CustomerDocument")->findOneBy([
            'customer' => $customer,
            'type' => $type
        ]);

        $status = $this->entityManager->getRepository("EnotApiBundle:CustomerDocumentStatus")->find(self::DOCUMENT_STATUS_ON_CHECK);
        $path = $this->loadFile($image, "customer_documents/");

        if(!$document) {
            $document = new CustomerDocument();
        }

        $document->setCustomer($customer);
        $document->setType($type);
        $document->setImage($path);
        $document->setStatus($status);
        $this->save($document);
        return $document;
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        /** @var EntityRepository $repository */
        $repository = $this->entityManager->getRepository('EnotApiBundle:CustomerDocumentType');
        return $repository;
    }

    /**
     * @param $entity
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function save($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush($entity);
    }

    /**
     * @param $documentId
     * @return string
     */
    public function showImage($documentId)
    {
        $document = $this->entityManager->getRepository("EnotApiBundle:CustomerDocument")->find($documentId);
        if(!$document->getImage()) {
            return null;
        }
        $url = 'https://u180511.your-storagebox.de/' . $document->getImage();
        return $this->get($url);
    }
}