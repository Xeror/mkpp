<?php

namespace Enot\ApiBundle\Controller;


use Enot\ApiBundle\Services\CustomerDocumentManager;
use Enot\ApiBundle\Services\CustomerManager;
use Enot\ApiBundle\Services\UserManager;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Post;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations as SWG;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;

class CustomerController extends BaseController
{
    /**
     * Возвращает информацию о пользователе
     *
     * ###Response
     * <pre>
     * {
     *      "request_id": "60D33608-938F-4AF3-932C-E9EF93FE53A4",
     *      "status_code": 200,
     *      "data": {
     *          "id": 91,
     *          "first_name": null,
     *          "second_name": null,
     *          "balance": 0,
     *          "id_tag": null,
     *          "user": {
     *              "id": 92,
     *              "username": "exampleLogin",
     *              "email": "example@example.com",
     *          }
     *      },
     *      "error": null
     * }
     * </pre>
     *
     * @Operation(
     *     tags={"Customer"},
     *     summary="Получение информации пользователя",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Get("/info")
     */
    public function getInfoAction()
    {
        try {
            /** @var UserManager $userService */
            $userService = $this->get('enot_api.user_manager');
            $user = $this->getUserEntity();
            $result = $userService->getCustomerInfo($user);
        } catch (\Exception $exception) {
            $result = $exception;
        }

        /** @var Response $response */
        $response = $this->get("enot_api.response_manager")->getResponse($result, ['Default']);
        return $response;
    }

    /**
     *
     * ###Response
     * <pre>
     * {
     *      "request_id": "E758B4BE-B7BD-4409-A111-A38E6C188B35",
     *      "status_code": 200,
     *      "data": {
     *          "id": 91,
     *          "first_name": null,
     *          "second_name": null,
     *          "balance": 0,
     *          "id_tag": null,
     *          "user": {
     *              "id": 92,
     *              "username": "exampleLogin",
     *              "email": "example@example.com",
     *          }
     *      },
     *      "error": null
     * }
     * </pre>
     *
     * @Post("/edit", name="customer_edit")
     * @Operation(
     *     tags={"Customer"},
     *     summary="Редактирование информации о пользователе",
     *     @SWG\Parameter(
     *         name="first_name",
     *         in="formData",
     *         description="Новое имя пользователя",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="second_name",
     *         in="formData",
     *         description="Новая фамилия пользователя",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @param Request $request
     * @return mixed
     */
    public function editAction(Request $request)
    {
        try {
            /** @var CustomerManager $manager */
            $manager = $this->get('enot_api.customer_manager');
            $result = $manager->setCustomer($this->getUserEntity()->getCustomer())->edit($request);

        } catch (\Exception $exception) {
            $result = $exception;
        }

        /** @var Response $response */
        $response = $this->get("enot_api.response_manager")->getResponse($result);
        return $response;
    }

    /**
     * Создание машины пользователя
     *
     * ###Response
     * <pre>
     * {
     *      "request_id": "60D33608-938F-4AF3-932C-E9EF93FE53A4",
     *      "status_code": 200,
     *      "data": {
     *          "id": 3,
     *          "model": {
     *              "id": 10,
     *              "brand": {
     *                  "name": "Kia"
     *              },
     *              "name": "Clarus"
     *          },
     *          "plate": "plate"
     *      },
     *      "error": null
     * }
     * </pre>
     *
     * @Operation(
     *     tags={"Customer"},
     *     summary="Создание машины пользователя",
     *     @SWG\Parameter(
     *         name="plate",
     *         in="formData",
     *         description="Номер машины",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="modelId",
     *         in="formData",
     *         description="Идентификатор модели машины",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @Post("/vehicle/add")
     * @param Request $request
     * @return Response
     */
    public function addVehicleAction(Request $request)
    {
        try {
            $modelId = $request->request->get("modelId");
            $carNumber = $request->request->get("carNumber");

            /** @var CustomerManager $manager */
            $manager = $this->get('enot_api.customer_manager');
            $result = $manager->setCustomer($this->getUserEntity()->getCustomer())
                ->addVehicle($modelId, $carNumber);

        } catch (\Exception $exception) {
            $result = $exception;
        }

        /** @var Response $response */
        $response = $this->get("enot_api.response_manager")->getResponse($result, ["Default"]);
        return $response;
    }

    /**
     * Удаление машины пользователя
     *
     * ###Response
     * <pre>
     * {
     *      "request_id": "60D33608-938F-4AF3-932C-E9EF93FE53A4",
     *      "status_code": 200,
     *      "data": true,
     *      "error": null
     * }
     * </pre>
     *
     * @Operation(
     *     tags={"Customer"},
     *     summary="Удаление машины пользователя",
     *     @SWG\Parameter(
     *         name="id",
     *         in="formData",
     *         description="Идентификатор машины",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @Get("/vehicle/remove/{id}")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function removeVehicleAction(Request $request, $id)
    {
        try {
            /** @var CustomerManager $manager */
            $manager = $this->get('enot_api.customer_manager');
            $result = $manager->setCustomer($this->getUserEntity()->getCustomer())->removeVehicle($id);

        } catch (\Exception $exception) {
            $result = $exception;
        }

        /** @var Response $response */
        $response = $this->get("enot_api.response_manager")->getResponse($result);
        return $response;
    }

    /**
     * Редактирование машины пользователя
     *
     * ###Response
     * <pre>
     * {
     *      "request_id": "60D33608-938F-4AF3-932C-E9EF93FE53A4",
     *      "status_code": 200,
     *      "data": {
     *          "id": 3,
     *          "model": {
     *              "id": 10,
     *              "brand": {
     *                  "name": "Kia"
     *              },
     *              "name": "Clarus"
     *          },
     *          "plate": "plate"
     *      },
     *      "error": null
     * }
     * </pre>
     *
     * @Operation(
     *     tags={"Customer"},
     *     summary="Редактирование машины пользователя",
     *     @SWG\Parameter(
     *         name="vehicleId",
     *         in="formData",
     *         description="Идентификатор машины",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="plate",
     *         in="formData",
     *         description="Номер машины",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="modelId",
     *         in="formData",
     *         description="Идентификатор модели машины",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @Post("/vehicle/edit")
     * @param Request $request
     * @return Response
     */
    public function editVehicleAction(Request $request)
    {
        try {
            $vehicleId = $request->request->get("vehicleId");
            $carNumber = $request->request->get("carNumber");
            $modelId = $request->request->get("modelId");

            /** @var CustomerManager $manager */
            $manager = $this->get('enot_api.customer_manager');
            $result = $manager->setCustomer($this->getUserEntity()->getCustomer())
                ->editVehicle($vehicleId, $modelId, $carNumber);

        } catch (\Exception $exception) {
            $result = $exception;
        }

        /** @var Response $response */
        $response = $this->get("enot_api.response_manager")->getResponse($result, ["Default"]);
        return $response;
    }

    /**
     * Загружает фотографии документов
     *
     * ###Response
     * <pre>
     * {
     *      "request_id": "679F5483-3902-42FE-A52C-8B93A89728D8",
     *      "status_code": 200,
     *      "data": {
     *          "id": 12,
     *          "image": "image.jpg"
     *      },
     *      "error": null
     * }
     * </pre>
     *
     * @Operation(
     *     tags={"Customer"},
     *     summary="Загрузка документов",
     *     @SWG\Parameter(
     *         name="typeId",
     *         in="formData",
     *         description="Document Type Identifier",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         name="image",
     *         in="formData",
     *         description="Image of vehicle defect",
     *         required=false,
     *         type="file"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @Post("/document/load", name="load_customer_document")
     * @param Request $request
     * @return mixed
     */
    public function loadAction(Request $request)
    {
        try {
            $user = $this->getUserEntity();

            /** @var CustomerDocumentManager $manager */
            $manager = $this->get('enot_api.customer_document_manager');

            $result = $manager->load($user->getCustomer(), $request);
        } catch (\Exception $exception) {
            $result = $exception;
        }

        /** @var Response $response */
        $response = $this->get("enot_api.response_manager")->getResponse($result, ["List"]);
        return $response;
    }

    /**
     * Редактирование моделей автомобиля
     *
     * @param $id
     * @return string
     * @Get("/document/image/{id}", name="show_document_image")
     */
    public function showImageAction($id)
    {
        $response = null;
        $manager = $this->get('enot_api.customer_document_manager');
        $response = $manager->showImage($id);
        echo $response;die();
    }
}