<?php

namespace Enot\ApiBundle\Controller;

use Enot\ApiBundle\Entity\User;
use FOS\RestBundle\Controller\FOSRestController;

class BaseController extends FOSRestController
{
    /**
     * @return User|null
     */
    protected function getUserEntity(): ?User
    {
        $user = null;
        if ($this->getUser()) {
            $user = $this->getDoctrine()->getRepository('EnotApiBundle:User')->find($this->getUser());
        }

        return $user;
    }
}
