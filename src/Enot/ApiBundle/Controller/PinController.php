<?php

namespace Enot\ApiBundle\Controller;


use Enot\ApiBundle\Services\UserManager;
use FOS\RestBundle\Controller\Annotations\Post;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;

class PinController extends BaseController
{
    /**
     * Отправляет код подтверждения на номер телефона клиента <br/>
     * Если пользователя в системе еще нет - создает нового пользователя
     *
     * ###Response
     * <pre>
     * {
     *      "request_id": "48330269-B040-440A-883B-ECB1D28675C3",
     *      "status_code": 200,
     *      "data": "success",
     *      "error": null
     * }
     * </pre>
     *
     * @Post("/")
     * @param Request $request
     * @Operation(
     *     tags={"Pin code"},
     *     summary="Регистрация или получение нового пин-кода",
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="Phone number for registration",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="country_code",
     *         in="formData",
     *         description="Country code by http://country.io/names.json",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pinAction(Request $request)
    {
        try {
            $countryCode = $request->request->get('country_code');
            $phone = $request->request->get('phone');

            /** @var UserManager $userManager */
            $userManager = $this->get('enot_api.user_manager');
            $result = $userManager->sendPinCode($countryCode, $phone, $this->container->getParameter('is_sms_send'));
        } catch (\Exception $exception) {
            $result = $exception;
        }

        /** @var Response $response */
        $response = $this->get("enot_api.response_manager")->getResponse($result);
        return $response;
    }

    /**
     * Подтверждает введенный пин-код
     *
     * ###Response
     * <pre>
     * {
     *      "request_id": "48330269-B040-440A-883B-ECB1D28675C3",
     *      "status_code": 200,
     *      "data": "success",
     *      "error": null
     * }
     * </pre>
     *
     * @Post("/confirm")
     * @param Request $request
     * @Operation(
     *     tags={"Pin code"},
     *     summary="Подтверждение пин-кода",
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="Phone number for registration",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="country_code",
     *         in="formData",
     *         description="Country code by http://country.io/names.json",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="code",
     *         in="formData",
     *         description="Code confirmation",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pinConfirmationAction(Request $request)
    {
        try {
            $countryCode = $request->request->get('country_code');
            $phone = $request->request->get('phone');
            $code = $request->request->get('code');

            $userManager = $this->get('enot_api.user_manager');
            $result = $userManager->confirmUser($countryCode, $phone, $code);
        } catch (\Exception $exception) {
            $result = $exception;
        }

        /** @var Response $response */
        $response = $this->get("enot_api.response_manager")->getResponse($result);
        return $response;
    }
}