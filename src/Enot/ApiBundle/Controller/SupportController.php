<?php
/**
 * ...
 */

namespace Enot\ApiBundle\Controller;


use Enot\ApiBundle\Services\SupportManager;
use FOS\RestBundle\Controller\Annotations\Post;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SupportController extends BaseController
{
    /**
     * @Post("/contact_email")
     * @Operation(
     *     tags={"Support"},
     *     summary="Отправка email-сообщения в техническую поддержку",
     *     @SWG\Parameter(
     *         name="title",
     *         in="formData",
     *         description="Email title",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="content",
     *         in="formData",
     *         description="Email content",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     )
     * )
     * @param Request $request
     * @return Response
     */
    public function contactEmailAction(Request $request)
    {
        try {
            $title = $request->request->get('title');
            $content = $request->request->get('content');
            $user = $this->getUserEntity();

            /** @var SupportManager $supportManager */
            $supportManager = $this->get('enot_api.support_manager');
            $result = $supportManager->contactEmail($user, $title, $content);
        } catch (\Exception $exception) {
            $result = $exception;
        }

        /** @var Response $response */
        $response = $this->get("enot_api.response_manager")->getResponse($result);
        return $response;
    }
}