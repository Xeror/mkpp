<?php
/**
 *
 */

namespace Enot\ApiBundle\Model\Interfaces;


use Enot\ApiBundle\Entity\User;

interface CustomerInterface
{
    /**
     * Get id
     *
     * @return integer
     */
    public function getId();

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName();

    /**
     * Get secondName
     *
     * @return string
     */
    public function getSecondName();

    /**
     * Get balance
     *
     * @return float
     */
    public function getBalance();

    /**
     * Get user
     *
     * @return User
     */
    public function getUser();

    /**
     * @return $this
     */
    public function getCustomer();
}