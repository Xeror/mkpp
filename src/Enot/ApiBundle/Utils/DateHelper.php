<?php
/**
 * ...
 */

namespace Enot\ApiBundle\Utils;


class DateHelper
{
    /**
     * Returns the difference between two dates in seconds
     *
     * @param \DateTime $dateTime1
     * @param \DateTime $dateTime2
     * @return float|int
     */
    public static function dateDiffSec(\DateTime $dateTime1, \DateTime $dateTime2)
    {
        $diff = date_diff($dateTime1, $dateTime2);
        $diffSeconds = $diff->s + $diff->i * 60 + $diff->h * 3600 + $diff->d * 86400;

        return $diffSeconds;
    }
}