<?php

namespace Enot\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Enot\ApiBundle\Model\Interfaces\CustomerInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * Customer
 *
 * @Serializer\ExclusionPolicy("none")
 * @ORM\Table(name="customers", indexes={@ORM\Index(name="user_id", columns={"user_id"})})
 * @ORM\Entity(repositoryClass="Enot\ApiBundle\Repository\CustomerRepository")
 */
class Customer implements CustomerInterface
{
    /**
     * @var integer
     *
     * @Serializer\Groups({"Default"})
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\Groups({"Default", "List"})
     * @ORM\Column(name="first_name", type="string", length=100, nullable=true)
     */
    private $firstName;

    /**
     * @var string
     *
     * @Serializer\Groups({"Default", "List"})
     * @ORM\Column(name="second_name", type="string", length=100, nullable=true)
     */
    private $secondName;

    /**
     * @var float
     *
     * @Serializer\Groups({"Default", "List"})
     * @ORM\Column(name="balance", type="float", precision=10, scale=0, nullable=false, options={"default":0})
     */
    private $balance = 9999;

    /**
     * @var User
     *
     *
     * @Serializer\Groups({"Default"})
     * @ORM\OneToOne(targetEntity="User", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Customer
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set secondName
     *
     * @param string $secondName
     *
     * @return Customer
     */
    public function setSecondName($secondName)
    {
        $this->secondName = $secondName;

        return $this;
    }

    /**
     * Get secondName
     *
     * @return string
     */
    public function getSecondName()
    {
        return $this->secondName;
    }

    /**
     * Set balance
     *
     * @param float $balance
     *
     * @return Customer
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Customer
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return $this
     */
    public function getCustomer()
    {
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVehicles()
    {
        return $this->vehicles;
    }

    /**
     * @param mixed $vehicles
     */
    public function setVehicles($vehicles): void
    {
        $this->vehicles = $vehicles;
    }

    /**
     * @return mixed
     */
    public function getDocuments()
    {
        return $this->documents;
    }

    /**
     * @param mixed $documents
     */
    public function setDocuments($documents): void
    {
        $this->documents = $documents;
    }
}
