<?php

namespace Enot\ApiBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Enot\ApiBundle\Entity\User;
use Enot\ApiBundle\Entity\Customer;

class CustomerRepository extends EntityRepository
{
    /**
     * @param User $user
     * @return Customer|object|null
     */
    public function findOneByUser(User $user)
    {
        return $this->findOneBy(['user' => $user]);
    }

    /**
     * @param string $idTag
     * @return Customer|object|null
     */
    public function findOneByIdTag(string $idTag)
    {
        return $this->findOneBy(['idTag' => $idTag]);
    }
}