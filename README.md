Dependencies
------------

* php ^7.2
* MySQL ^5.7


Installation
------------

* git pull
* composer install
* set app/config/parameters.yml
* bin/console cac:cl --env=prod
* bin/console cac:war --env=prod
* chmod 777 var/ -R
